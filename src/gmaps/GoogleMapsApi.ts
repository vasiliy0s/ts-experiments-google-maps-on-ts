import { Loader as GoogleMapsApiLoader, google } from 'google-maps'

const secureApiKeySymbol = Symbol('secureApiKeySymbol')

export class GoogleMapsApi {
  private static google: google = null
  private static [secureApiKeySymbol]: string = null

  static async setup(apiKey: string) {
    this[secureApiKeySymbol] = apiKey
  }

  static async get(): Promise<google> {
    if (!this.google) {
      await this.loadGoogleMaps()
    }

    return this.google
  }

  private static async loadGoogleMaps() {
    const loader = new GoogleMapsApiLoader(this.getApiKey())
    this.google = await loader.load()
  }

  private static getApiKey() {
    if (!this[secureApiKeySymbol]) {
      throw new Error(
        'There is no apiKey for GoogleMapsApi! Call GoogleMapsApi.setup(apiKey) first'
      )
    }

    return this[secureApiKeySymbol]
  }
}
