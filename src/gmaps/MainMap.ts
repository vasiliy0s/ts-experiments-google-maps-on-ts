import { CoordRange } from '../types'
import { GoogleMapsApi } from './GoogleMapsApi'
import { Mappable, Map } from '../interfaces'

export class MainMap implements Map {
  private gMap: google.maps.Map
  private markers: google.maps.Marker[] = []
  private openInfoWindow: google.maps.InfoWindow

  constructor(
    private mapElementSelector: string,
    private initialMapArea: CoordRange,
    private initialZoom = 8
  ) {}

  addMarker(mappable: Mappable): void {
    const marker = this.createMarker(mappable)
    const infoWindow = this.createInfoWindow(mappable)
    this.handleMarkerClick(marker, infoWindow)
  }

  private createMarker({ location, name }: Mappable): google.maps.Marker {
    const marker = new google.maps.Marker({
      position: location,
      title: name,
    })

    marker.setMap(this.gMap)
    this.markers.push(marker)

    return marker
  }

  private createInfoWindow(mappable: Mappable): google.maps.InfoWindow {
    return new google.maps.InfoWindow({
      content: mappable.markerContent(),
    })
  }

  private handleMarkerClick(
    marker: google.maps.Marker,
    infoWindow: google.maps.InfoWindow
  ) {
    marker.addListener('click', () => {
      if (this.openInfoWindow === infoWindow) return
      this.tryCloseInfoWindow()
      infoWindow.open(this.gMap, marker)
      this.openInfoWindow = infoWindow
    })
  }

  clear() {
    this.clearMarkers()
    this.tryCloseInfoWindow()
  }

  private tryCloseInfoWindow() {
    this.openInfoWindow?.close()
  }

  private clearMarkers() {
    for (const marker of this.markers) {
      marker.setMap(null)
      marker.unbindAll()
    }
    this.markers.splice(0, this.markers.length)
  }

  async init() {
    const google = await GoogleMapsApi.get()

    const mapEl = document.querySelector(this.mapElementSelector)
    const center = this.getGMapCenter(this.initialMapArea)

    this.gMap = new google.maps.Map(mapEl, {
      center,
      zoom: this.initialZoom,
    })
  }

  private getGMapCenter([
    latRange,
    lngRange,
  ]: CoordRange): google.maps.LatLngLiteral {
    const lat =
      Math.min(latRange[0], latRange[1]) +
      Math.abs(latRange[1] - latRange[0]) / 2
    const lng =
      Math.min(lngRange[0], lngRange[1]) +
      Math.abs(lngRange[1] - lngRange[0]) / 2

    return { lat, lng }
  }
}
