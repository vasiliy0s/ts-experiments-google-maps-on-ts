import { CoordRangeTuple } from './types'

export const LAT: CoordRangeTuple = [
  parseFloat(process.env.LAT_RANGE_MIN),
  parseFloat(process.env.LAT_RANGE_MAX),
]

export const LNG: CoordRangeTuple = [
  parseFloat(process.env.LNG_RANGE_MIN),
  parseFloat(process.env.LNG_RANGE_MAX),
]

export const GOOGLE_API_KEY = String(process.env.GOOGLE_API_KEY || '')
