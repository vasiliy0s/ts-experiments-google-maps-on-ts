import './styles.scss'

import { Map } from './interfaces'
import { MainMap } from './gmaps'
import { configure } from './configure'
import { PointsGenerator } from './classes'

const handleGenerateButtonClick = (pg: PointsGenerator) => {
  document.querySelector('#generate').addEventListener('click', () => {
    pg.regenerate()
  })
}

export const createApp = async () => {
  const { area } = configure()

  const map: Map = new MainMap('#map', area, 10)
  await map.init()

  const pg = new PointsGenerator(map, area)
  pg.generate()
  handleGenerateButtonClick(pg)
}
