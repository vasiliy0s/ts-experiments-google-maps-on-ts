import { GOOGLE_API_KEY, LAT, LNG } from './config'
import { GoogleMapsApi } from './gmaps'
import { CoordRange } from './types'

export const configure = () => {
  GoogleMapsApi.setup(GOOGLE_API_KEY)

  const area: CoordRange = [LAT, LNG]

  return {
    area,
  }
}
