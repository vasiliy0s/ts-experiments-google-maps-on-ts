export interface MapLocation {
  lat: number
  lng: number
  print(): string
}

export interface Mappable {
  location: MapLocation
  name: string
  color: string
  markerContent(): string
}
