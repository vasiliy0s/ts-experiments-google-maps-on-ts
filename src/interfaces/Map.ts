import { Mappable } from './Mappable'

export interface Map {
  init(): Promise<void>
  addMarker(item: Mappable): void
  clear(): void
}
