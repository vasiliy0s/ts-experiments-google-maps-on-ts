import { address as fakerAddress } from 'faker'
import { format } from 'util'
import { MapLocation } from '../interfaces'
import { CoordRange } from '../types'

export class Coordinates implements MapLocation {
  static fromRange([latRange, longRange]: CoordRange) {
    return new Coordinates(
      parseFloat(fakerAddress.latitude(latRange[0], latRange[1])),
      parseFloat(fakerAddress.longitude(longRange[0], longRange[1]))
    )
  }

  constructor(public lat: number, public lng: number) {}

  print(): string {
    return format('coordinates-[%s, %s]', this.lat, this.lng)
  }
}
