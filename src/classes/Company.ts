import { company as fakerCompany } from 'faker'
import { Coordinates } from './Coordinates'
import { CoordRange } from '../types'
import { Mappable, MapLocation } from '../interfaces'

export class Company implements Mappable {
  name: string
  catchPhrase: string
  location: MapLocation
  color = 'blue'

  constructor(area: CoordRange, nameFormatType = 0) {
    this.name = fakerCompany.companyName(nameFormatType)
    this.catchPhrase = fakerCompany.catchPhrase()
    this.location = Coordinates.fromRange(area)
  }

  markerContent() {
    return `
      <h3>${this.name}</h3>
      <p>${this.catchPhrase}</p>
    `
  }
}
