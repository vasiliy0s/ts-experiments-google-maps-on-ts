import { name as fakerName } from 'faker'
import { Coordinates } from './Coordinates'
import { CoordRange } from '../types'
import { Mappable, MapLocation } from '../interfaces'

export class User implements Mappable {
  name: string
  location: MapLocation
  color = 'blue'

  constructor(area: CoordRange) {
    const gender = Math.random() > 0.5 ? 1 : 0
    this.name = `${fakerName.firstName(gender)} ${fakerName.lastName(gender)}`
    this.location = Coordinates.fromRange(area)
  }

  markerContent() {
    return `
      <h3>${this.name}</h3>
    `
  }
}
