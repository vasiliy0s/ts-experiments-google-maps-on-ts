import { CoordRange } from '../types'
import { Company } from './Company'
import { User } from './User'
import { Map } from '../interfaces'

export class PointsGenerator {
  constructor(private map: Map, private area: CoordRange) {}

  generate() {
    const user = new User(this.area)
    const company = new Company(this.area)

    console.log('Hi %s', user.name)
    console.log('Are you at [%s, %s]?', user.location.print())
    console.log('Are you working at %s?', company.name)

    this.map.addMarker(user)
    this.map.addMarker(company)
  }

  regenerate() {
    this.map.clear()
    this.generate()
  }
}
