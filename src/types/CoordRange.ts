export type CoordRangeTuple = [number, number]

export type CoordRange = [lat: CoordRangeTuple, lng: CoordRangeTuple]
