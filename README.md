# TypeScript Experiments

This is an expreimental repository for TypeScript advanced learning.

## Installation

1. Clone this repo with git
2. Install dependencies with [Yarn](https://yarnpkg.com/):

```sh
yarn install
```

## Usage

```sh
yarn build # build to `dist` folder with [Parcel](https://parceljs.org/)
yarn serve:dist # open built project in a broswer using [lite-server](https://www.npmjs.com/package/lite-server)
```

## Development

```sh
yarn dev # run local development server with [Parcel](https://parceljs.org/)
```
